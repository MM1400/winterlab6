public class Board {
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	Board() {
		die1 = new Die();
		die2 =  new Die();
		tiles = new boolean[12];
	}
	public String toString() {
		String result = "";
		for(int i =0; i < this.tiles.length; i++) {
			if(tiles[i]) {
				result+=i;
			} else {
				result+="X";
			}
			result+= " ";
		}
		return result;
	}
	public boolean playATurn() {
		die1.roll();
		die2.roll();
		System.out.println("Value of die1 is : " + die1);
		System.out.println("Value of die2 is : " + die2);
		int sumOfDice = die1.getFaceValue() + die2.getFaceValue();
		if(!(tiles[sumOfDice])) {
			System.out.println("Closing the tile equal to sum: " + sumOfDice);
			tiles[sumOfDice] = true;
			return false;
		}else if(!(tiles[die1.getFaceValue()])) {
			System.out.println("Closing the tile equal to die 1: " + die1.getFaceValue());
			tiles[die1.getFaceValue()] = true;
			return false;
		}else if(!(tiles[die2.getFaceValue()])) {
			System.out.println("Closing the tile equal to die 2: " + die2.getFaceValue());
			tiles[die2.getFaceValue()] = true;
			return false;
		}
		System.out.println("All the tiles for these values are already shut");
		return true;
		}
	}
