public class Jackpot {
	public static void main(String[] args) {
	System.out.println("Welcome to Jackpot");
	Board board = new Board();
	boolean gameOver = false;
	int numOfTilesClosed = 0;
	while(!gameOver) {
		System.out.println(board);
			if(board.playATurn()) {
				gameOver = true;
			} else {
				numOfTilesClosed++;
			}
		}
		if(numOfTilesClosed>=7) {
			System.out.println("You have reached the jackpot and have won!");
		}else {
			System.out.println("You have not reached the jackpot and have lost");
		}
	}
}
